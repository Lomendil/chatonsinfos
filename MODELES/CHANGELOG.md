# ChatonsInfos Changelog

All notable changes of ChatonsInfos will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]
### Added
- software.modules : liste de modules optionnels installés (type VALUES, optionnel, ex. Nextcloud-Calendar,Nextcloud-Talk).
- organization.chatrooms.foo : lien du salon public foo de l'organisation (type URL, optionnel).
- organization.memberof.chatons.startdate : date d'entrée dans le collectif (type DATE, obligatoire, ex. 08/11/2018).
- organization.memberof.chatons.enddate : date de sortie du collectif (type DATE, optionnel, ex. 08/11/2019).
- organization.memberof.chatons.status.level : statut en tant que membre de l'organisation (un parmi {ACTIVE, IDLE, AWAY}, obligatoire).
- organization.memberof.chatons.status.description : description du statut en tant que membre de l'organisation (type STRING, optionnel, ex. en sommeil).

### Changed
- host.description : recommandé -> optionnel
- federation.socialnetworks.*: recommandé -> optionnel
- organization.socialnetworks.*: recommandé -> optionnel
### Deprecated
### Removed
### Fixed


## [0.1] - 2020-11-23
### Added
- all
### Changed
### Deprecated
### Removed
### Fixed

